﻿public static class TextMessages
{
    public const string RunMessage = "Run";
    public const string CantOpenFileErrorMessage = "cant open file with name: {0} ";
    public const string InvitingCharacter = ">";
    public const string SearchOuputFormat = " {0}";
    public const string UnknownArguments = "Unknown arguments: {0}";
    public const string IncorrectFileNameMessage = "Incorrect filename";
    public const string IncorrectCommandMessage = "IncorrectCommand"; 
    public const string IncorrectCommandNameMessage = "Incorrect command name";
    public const string NeedOpenFileMessage = "for callculate checksumm you must open file";
    public const string ResultCheckSumMessage = "Result check summ: {0}";
    public const string WordOffsetResultMessage = "Word: {0} in file with name: {1} found with offsets:";
    public const string FirstMessage = "input \"Run -h\" for call help /n input \"Quit\" or press Ctrl+C to Exit ";

    public const string ManualMessage = "1) Run –f filename –m find –s hello " +
                                        "\n Выводит все смещения в байтах (через пробел) для файла «filename»," +
                                        "\n где расположена строка «hello» \n" +
                                        "2) Run –f filename –m checksum \n " +
                                        "Выводит 32-хбитную чексумму, рассчитанную по сумме всех 32-хбитных слов в файле. \n" +
                                        "3) Run –h \n" +
                                        " Выводит справку о командах и параметрах \n" +
                                        "4) Clear - очистить консоль\n";
}
