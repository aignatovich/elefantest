﻿using System;

public class StringUtils
{
    private static readonly char[] argsSeparators = {' '};
  //  private static readonly string[] fileNameSeparators = {"-f", "-m"};

    public static string[] SplitString( string sourceString )
    {
        return sourceString.Split( argsSeparators, StringSplitOptions.RemoveEmptyEntries );
    }

   /* public static string FindFileNameFromString( string sourceString  )
    {
        var startIndex = sourceString.IndexOf( fileNameSeparators[0] );
        var endIndex = sourceString.IndexOf( fileNameSeparators[1] );

        if ( startIndex < endIndex && startIndex > endIndex )
        {
            return  sourceString.Split( fileNameSeparators, StringSplitOptions.None )[1];
        }

        return string.Empty;
    }*/
}
