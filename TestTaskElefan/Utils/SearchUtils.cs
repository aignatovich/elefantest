﻿using System.Collections.Generic;

internal class SearchUtils
{
    public static List<long> SearchBytes(byte[] sourceArray, byte[] searchedArray)
    {
        List<long> result = new List<long>();
        for ( int i = 0; i <= sourceArray.Length - searchedArray.Length; i++ )
        {
            if ( Match( sourceArray, searchedArray, i ) )
            {
                result.Add( i );
            }
        }

        return result;
    }

    private static bool Match( byte[] sourceArray, byte[] searchedArray, int start )
    {
        if ( searchedArray.Length + start > sourceArray.Length )
        {
            return false;
        }

        for ( int i = 0; i < searchedArray.Length; i++ )
        {
            if ( searchedArray[i] != sourceArray[i + start] )
            {
                return false;
            }
        }

        return true;
    }
}
