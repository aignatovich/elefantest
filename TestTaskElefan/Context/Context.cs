﻿public class Context
{
    private string source;
    private BaseCommand result;

    public string Source
    {
        get { return source; }
        set { source = value.Trim(); }
    }

    public BaseCommand Result
    {
        get { return result; }
        set { result = value; }
    }

    public Context( string source )
    {
        this.source = source;
    }
}
