﻿using System;

public class Program
{
    private static BaseInterpreter[] interpreters =
    {
        new StartInterpreter(),
        new HelpInterpreter(),
        new GetFileInterpreter(),
        new FindInterpreter(),
        new CheckSumInterpreter()
    };

    private static void Main( string[] args )
    {
        Console.WriteLine( TextMessages.FirstMessage );
        Console.Write( TextMessages.InvitingCharacter );

        string line;
        while ( true )
        {
            line = Console.ReadLine();
            if ( string.IsNullOrEmpty( line ) )
            {
                Console.Write( TextMessages.InvitingCharacter );
                continue;
            }

            Context context = new Context( line );
            for ( int i = 0; i < interpreters.Length; ++i )
            {
                interpreters[i].Interpret( context );
            }

            if ( !string.IsNullOrEmpty( context.Source ) )
            {
                Console.WriteLine( TextMessages.UnknownArguments, context.Source );
                Console.Write( TextMessages.InvitingCharacter );
                continue;
            }

            context.Result.Execute();
            Console.Write( TextMessages.InvitingCharacter );
        }
    }
}
