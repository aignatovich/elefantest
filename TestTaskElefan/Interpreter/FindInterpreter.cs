﻿public class FindInterpreter : BaseInterpreter
{
    private string findToken = "find";
    private string searchedWordToken = "-s";

    public override void Interpret( Context context )
    {

        if ( context.Source.Length == 0 )
        {
            return;
        }

        if ( context.Source.StartsWith( findToken ) )
        {
            string searchedWord = context.Source.Substring( context.Source.IndexOf( searchedWordToken ) + searchedWordToken.Length ).Trim();
            context.Source = context.Source.Substring( context.Source.IndexOf( searchedWord ) + searchedWord.Length );
            context.Result = new SearchWordCommand( context.Result, searchedWord );
        }
    }
}
