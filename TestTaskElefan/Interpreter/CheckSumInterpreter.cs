﻿internal class CheckSumInterpreter : BaseInterpreter
{
    private string checksumToken = "checksum";

    public override void Interpret( Context context )
    {
        if ( context.Source.Length == 0 )
        {
            return;
        }

        if ( context.Source.StartsWith( checksumToken ) )
        {
            context.Result = new CalculateCheckSummCommand( context.Result );
            context.Source = context.Source.Substring( checksumToken.Length );
        }
    }
}
