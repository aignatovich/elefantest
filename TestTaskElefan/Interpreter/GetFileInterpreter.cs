﻿using System;

public class GetFileInterpreter : BaseInterpreter
{
    private string startFilenameToken = "-f";
    private string endFileNameToken = "-m";

    public override void Interpret( Context context )
    {
        if ( context.Source.Length == 0 )
        {
            return;
        }

        if ( context.Source.StartsWith( startFilenameToken ) )
        {
            if ( context.Source.IndexOf( endFileNameToken ) == -1 )
            {
                Console.WriteLine( TextMessages.IncorrectCommandMessage );
                return;
            }

            string filename = context.Source.Substring( startFilenameToken.Length, context.Source.IndexOf( endFileNameToken ) - startFilenameToken.Length );
            Console.WriteLine( "Debug: filename:" + filename );
            context.Source = context.Source.Substring( filename.Length + startFilenameToken.Length + endFileNameToken.Length );
            context.Result = new OpenFileCommand( context.Result, filename );
        }
    }
}
