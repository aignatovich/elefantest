﻿public class StartInterpreter : BaseInterpreter
{
    private string quitToken = "Quit";
    private string runToken = "Run";
    private string ClearToken = "Clear";

    public override void Interpret( Context context )
    {
        if ( context.Source.Length == 0 )
        {
            return;
        }

        if ( context.Source.StartsWith( quitToken ) )
        {
            context.Result = new QuitCommand( context.Result );
            return;
        }

        if ( context.Source.StartsWith( ClearToken ) )
        {
            context.Result = new ClearCommand( context.Result );
            context.Source = context.Source.Substring( ClearToken.Length );
            return;
        }

        if ( context.Source.StartsWith( runToken ) )
        {
            context.Source = context.Source.Substring( runToken.Length );
            context.Result = new RunCommand( context.Result );
        }
    }
}
