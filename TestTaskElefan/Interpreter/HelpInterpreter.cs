﻿public class HelpInterpreter : BaseInterpreter
{
    private string helpToken = "-h";

    public override void Interpret( Context context )
    {
        if ( context.Source.Length == 0 )
        {
            return;
        }

        if ( context.Source.StartsWith( helpToken ) )
        {
            context.Result = new HelpCommand( context.Result );
            context.Source = context.Source.Substring( helpToken.Length );
        }
    }
}
