﻿using System.IO;
using System;
using System.Collections.Generic;
using System.Text;

internal class SearchWordCommand : BaseCommand
{
    private string searchedString;

    public SearchWordCommand( BaseCommand previos, string searchedString ) : base( previos )
    {
        this.searchedString = searchedString;
    }

    public override bool Execute()
    {
        if( !( previos is OpenFileCommand ) )
        {
            Console.WriteLine( TextMessages.NeedOpenFileMessage );
            return false;
        }

        if( !previos.Execute() )
        {
            return false;
        }

        List<long> offsets = GetOffsets();
        WriteSearchReport( offsets );

        return true;
    }

    private List<long> GetOffsets()
    {
        List<long> searchResult = new List<long>();

        using (var file = File.Open( ( previos as OpenFileCommand ).filePath, FileMode.Open, FileAccess.Read ))
        {
            int offset = 0;
            var searchedArray = Encoding.ASCII.GetBytes( searchedString );
            var buff = new byte[Constants.MaxByteArraySize];

            while (file.Length > offset)
            {
                if( file.Length - offset < Constants.MaxByteArraySize )
                {
                    buff = new byte[file.Length - offset];
                }

                file.Read( buff, offset, offset + buff.Length );
                offset += buff.Length;

                var positions = SearchUtils.SearchBytes( buff, searchedArray );
                foreach (var pos in positions)
                {
                    searchResult.Add( pos + offset );
                }
            } 
        }

        return searchResult;
    }

    private void WriteSearchReport( List<long> offsets )
    {
        StringBuilder log = new StringBuilder();
        log.Append( String.Format( TextMessages.WordOffsetResultMessage, searchedString, ( previos as OpenFileCommand ).filePath ) );
        log.AppendLine();

        for ( int i = 0; i < offsets.Count; ++i )
        {
            log.Append( string.Format( TextMessages.SearchOuputFormat, offsets[i] ) );
        }

        log.AppendLine();
        Console.WriteLine( log );
    }
}