﻿using System;
using System.IO;

internal class CalculateCheckSummCommand : BaseCommand
{
    public CalculateCheckSummCommand( BaseCommand previos ) : base( previos )
    {
    }

    public override bool Execute()
    {
        if( !( previos is OpenFileCommand ) )
        {
            Console.WriteLine( TextMessages.NeedOpenFileMessage );
            return false;
        }

        if( !previos.Execute() )
        {
            return false;
        }

        previos.Execute();

        var buff = new byte[Constants.MaxByteArraySize];
        bool flag;
        UInt32 resultSum = 0;

        using (var file = File.Open( ( previos as OpenFileCommand ).filePath, FileMode.Open, FileAccess.Read ))
        {
            int offset = 0;
            while (file.Length > offset)
            {
                if( file.Length - offset < Constants.MaxByteArraySize )
                {
                    buff = new byte[file.Length - offset];
                }

                file.Read( buff, 0, buff.Length );
                offset += buff.Length;
                for ( int i = 0; i < buff.Length; ++i )
                {
                    resultSum += BitConverter.ToUInt32( buff, buff.Length - i > 3 ? i : buff.Length - i );
                    i += 3;
                }
            }
        }

        Console.WriteLine( TextMessages.ResultCheckSumMessage, resultSum );
        return true;
    }
}