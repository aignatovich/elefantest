﻿using System;

internal class QuitCommand : BaseCommand
{
    public QuitCommand( BaseCommand previos ) : base( previos )
    {
        Environment.Exit( 0 );
    }
}
