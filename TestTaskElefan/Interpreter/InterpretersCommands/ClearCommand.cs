﻿using System;

public class ClearCommand : BaseCommand
{
    public ClearCommand( BaseCommand previos ) : base( previos )
    {
        Console.Clear();
    }
}
