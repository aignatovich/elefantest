﻿public class BaseCommand
{
    protected BaseCommand previos;

    public BaseCommand( BaseCommand previos )
    {
        this.previos = previos;
    }

    public virtual bool Execute()
    {
        return true;
    }
}
