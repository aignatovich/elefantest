﻿using System;
using System.IO;

internal class OpenFileCommand : BaseCommand
{
    public string filePath;

    public OpenFileCommand( BaseCommand previos, string filePath ) : base( previos )
    {
        this.filePath = filePath;
    }

    public override bool Execute()
    {
        if ( previos != null && !previos.Execute() )
        {
            return false;
        }

        FileStream fs = null;
        try
        {
            fs = new FileStream( filePath, FileMode.Open );
        }
        catch
        {
            Console.WriteLine( TextMessages.CantOpenFileErrorMessage, filePath );
            return false;
        }
        finally
        {
            if ( fs != null )
            {
                fs.Close();
            }
        }

        return true;
    }
}
