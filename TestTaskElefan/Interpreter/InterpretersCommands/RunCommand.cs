﻿    
using System;

internal class RunCommand : BaseCommand
{
    public RunCommand( BaseCommand previos ) : base( previos )
    {
        Console.WriteLine( TextMessages.RunMessage );
    }
}
