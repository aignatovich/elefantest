﻿using System;

public class HelpCommand : BaseCommand
{
    public HelpCommand( BaseCommand previos ) : base( previos )
    {
    }

    public override bool Execute()
    {
        if ( previos != null && !previos.Execute() )
        {
            return false;
        }

        Console.WriteLine( TextMessages.ManualMessage );
        return true;
    }
}
