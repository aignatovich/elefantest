﻿using System;

public class Interpreter
{
    private BaseCommand result;

    public void Parse( string sourceLine )
    {
        if ( string.IsNullOrWhiteSpace( sourceLine ) )
        {
            Console.Write( TextMessages.InvitingCharacter );
            return;
        }

        string[] args = StringUtils.SplitString( sourceLine );

        for ( int i = 0; i < args.Length; ++i )
        {
            switch ( args[i] )
            {
                case "Quit":
                    result = new QuitCommand( null );
                    break;

                case "Run":
                    result = new RunCommand( null );
                    break;

                case "-f":
                    if ( i + 1 >= args.Length )
                    {
                        Console.WriteLine( TextMessages.IncorrectFileNameMessage );
                    }

                    result = new OpenFileCommand( result, args[i + 1] );
                    i++;
                    break;

                case "-h":
                    result = new HelpCommand( result );
                    break;

                case "-m":
                    if ( i + 1 >= args.Length )
                    {
                        Console.WriteLine( TextMessages.IncorrectCommandNameMessage );
                    }

                    switch ( args[i + 1] )
                    {
                        case "checksum":

                            result = new CalculateCheckSummCommand( result );
                            i++;
                            break;

                        case "find":
                            if ( i + 2 >= args.Length || string.IsNullOrEmpty( args[i + 3] ) )
                            {
                                Console.WriteLine( TextMessages.IncorrectCommandNameMessage );
                            }

                            result = new SearchWordCommand( result, args[i + 3] );
                            i += 3;
                            break;
                    }

                    break;

                default:
                    Console.WriteLine( TextMessages.IncorrectCommandNameMessage  + args[i]);
                    Console.Write( TextMessages.InvitingCharacter );
                    result = null;
                    return;
            }
        }

        result.Execute();
        Console.Write( TextMessages.InvitingCharacter );
        result = null;
    }
}
